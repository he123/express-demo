var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/status', function (req, res) {
  res.send({status: 'ok'});
});
router.get('/ab?c', function (req, res) {
  console.log('matched；ab?c')
  res.send({status: 'ab?c'});
});





module.exports = router;
