var express = require('express');
var router = express.Router();
var http = require("http");
var https = require("https");
var fs = require("fs");

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

// xxx/users/testHttps 这个需要证书，总是提示版本错误
router.get('/testHttps', (req, response) => {
    var options = {
        host: '127.0.0.1',
        port: '12649',
        path: 'https://www.google.com',
        method: 'post',
    }
    options.agent = new https.Agent(options);
    https.request(options, (res) => {
        console.log('状态码:', res.statusCode);
        console.log('请求头:', res.headers);

        // res.on('data', (d) => {
        //     process.stdout.write(d);
        // });
        res.on('data', function (d) {
            body += d;
        }).on('end', function () {
            console.log(res.headers)
            console.log(body)
        });

    }).on('error', (e) => {
        console.error(e);
    });

})

router.get('/tests', function (req, response) {
    var opt = {
        host: '127.0.0.1',
        port: '12639',
        path: 'https://www.google.com',
        // path:'https://easy-mock.com/mock/5bb038cd4c5da84425d32073/example/mock',
        // method: 'POST',//这里是发送的方法
        method: 'CONNECT',
        //这里是访问的路径
        headers: {
            //这里放期望发送出去的请求头
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
    //以下是接受数据的代码
    var body = '';
    var req = http.request(opt, function (res) {
        console.log("Got response: " + res.statusCode);
        res.setEncoding('UTF-8');
        res.on('data', function (d) {
            body += d;
        });
        res.on('end', function () {
            response.send(body)

        });

    }).on('error', function (e) {
        console.log("Got error: " + e.message);
    });

    // req.end();
})

// xxx/users/testHttp
router.get('/testHttp', function (req, response) {
    let rawData = '';
    console.log("请求http");

    url = 'http://nodejs.cn/';
    // //配置代理,现在只能是http
    var options = {
        host: '127.0.0.1',
        port: '12639',
        path: url,
        method: 'get'
    }
    http.get(options, (res) => {
        const {statusCode} = res;
        const contentType = res.headers['content-type'];

        let error;
        if (statusCode !== 200) {
            error = new Error('请求失败\n' +
                `状态码: ${statusCode}`);
        }
        // else if (!/^application\/json/.test(contentType)) {
        //     error = new Error('无效的 content-type.\n' +
        //         `期望的是 application/json 但接收到的是 ${contentType}`);
        // }
        if (error) {
            console.error(error.message);
            // 消费响应数据来释放内存。
            res.resume();
            return;
        }

        res.setEncoding('utf8');

        res.on('data', (chunk) => {
            rawData += chunk;
        });
        res.on('end', () => {
            try {
                // const parsedData = JSON.parse(rawData);
                // console.log('===',rawData);
                response.send(rawData);
            } catch (e) {
                console.error(e.message);
            }
        });
    }).on('error', (e) => {
        console.error(`出现错误: ${e.message}`);
    });


});


module.exports = router;
